# Laravel8マルチ認証設定プロジェクト

Laravel8でlaravel/uiを使用し、ユーザ側と管理画面でそれぞれログインできるまでの初期設定
が終わった時点のプロジェクトです。


以下のページをベースに書いてある通り進めていき、一部そのままで上手くいかなかったところを
修正しながら、どうにか動いたかなというところでコミットしました。

https://reffect.co.jp/laravel/laravel-multi-authentication-understand#i-13
https://pgmemo.tokyo/data/archives/1483.html

# 筆者の開発環境
MacOS X BigSur M1チップ
Laravel8
Docker version 20.10.7, build f0df350
